from django.db import models
from users.models import User

# Create your models here.

class Partner(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE, related_name = 'partneruserid' )
    partner_rating = models.FloatField(default = 0.0)

class Coupon(models.Model):
    partner = models.ForeignKey(Partner, on_delete = models.CASCADE, default = 1)
    coupon_code = models.CharField(max_length = 50)
    coupon_percentage = models.IntegerField()
    description = models.CharField(max_length = 500)
    start_date = models.DateField()
    end_date = models.DateField()
    
    def __str__(self):
        return str(self.id)

CATEGORY_CHOICES = (
    ('Mobile', 'Mobile'),
    ('Laptop', 'Laptop'),
    ('Top Wear', 'Top Wear'),
    ('Bottom Wear', 'Bottom Wear'),
)

class Category(models.Model):
    category_name = models.CharField(max_length = 20) 
    description = models.TextField()
    
    def __str__(self):
        return str(self.id)


class Product(models.Model):
    partner = models.ForeignKey(Partner, on_delete = models.CASCADE, default=1)
    coupon = models.ForeignKey(Coupon, blank = True, null = True, on_delete = models.CASCADE, default=0)
    category = models.ForeignKey(Category, on_delete = models.CASCADE)
    name = models.CharField(max_length = 100) 
    picture = models.ImageField(upload_to = 'productimg')
    rating = models.FloatField(default = 0.0)
    description = models.TextField()
    MFD = models.DateField(blank = True, null = True)
    price =  models.DecimalField(max_digits = 10, decimal_places = 2)
    
    def __str__(self):
        return str(self.id)
    
    
    
    
