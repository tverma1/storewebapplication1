from django import forms
from django.core import validators
from django.http import request
from django.utils.translation import gettext, gettext_lazy as _
from partners.models import Product, Coupon

def price_validator(value):
    if value <= 0:
        raise forms.ValidationError('price should be positive and nonzero')



class ProductAddForm(forms.ModelForm):
    #coupon = forms.ModelChoiceField(label = 'Coupon ID', widget = forms.ModelChoiceField(attrs={'class': 'form-control'}))
    #category = forms.ModelChoiceField(label = 'Category IDs', widget = forms.ModelChoiceField(attrs={'class': 'form-control'}))
    name = forms.CharField(label = 'Name', widget = forms.TextInput(attrs={'class': 'form-control'}))
    picture = forms.ImageField(label = 'Picture', widget = forms.ClearableFileInput(attrs={'class': 'form-control'}))
    description = forms.CharField(label = 'Description', widget = forms.TextInput(attrs={'class': 'form-control'}))
    MFD = forms.DateField(label = 'MFD', widget = forms.DateInput(attrs={'class': 'form-control'}))
    price = forms.FloatField(label = 'Price', validators = [price_validator] , widget = forms.NumberInput(attrs={'class': 'form-control'}))
    class Meta:
        model = Product
        fields = ['coupon', 'category', 'name', 'picture', 'description','MFD', 'price',]
       
class AddCoupanForm(forms.ModelForm):
    class Meta:
        model = Coupon
        fields = ['coupon_code', 'coupon_percentage', 'description', 'start_date', 'end_date']