from django.http.response import HttpResponse, HttpResponseForbidden, HttpResponseRedirect, HttpResponseServerError
from django.shortcuts import render
from partners.models import Partner, Product, Coupon
from employees.models import Employee
from .forms import ProductAddForm, AddCoupanForm
from django.contrib import messages
from django.views import View
from django.db.models import Q
#from django.core.urlresolvers import reverse

# Create your views here.

def partner_dashboard(request):
    return render(request, 'partners/partnerdashboard.html')

# adding product
class AddProduct(View):
  
    def get(self, request, id):
        if not request.user.user_type == 'Partner':
            return HttpResponseForbidden()
        form = ProductAddForm()
        return render(request, 'partners/addproduct.html' , {'form': form})
    
    def post(self, request, id):
        if not request.user.user_type == 'Partner':
                return HttpResponseForbidden()
        partner_id = Partner.objects.get(user__id = id)
        form = ProductAddForm(request.POST, request.FILES)
        if form.is_valid():
            coupon = form.cleaned_data['coupon']
            category = form.cleaned_data['category']
            name = form.cleaned_data['name']
            picture = form.cleaned_data['picture']
            description = form.cleaned_data['description']
            MFD = form.cleaned_data['MFD']
            price = form.cleaned_data['price']
            save_product = Product(partner = partner_id, coupon = coupon, category = category, name = name, picture = picture, description = description, MFD = MFD, price = price)
            save_product.save()
            messages.success(request, 'Congratulation!! Added Product Successfully')
            form = ProductAddForm()
        return render(request, 'partners/addproduct.html', {'form':form})
    
# partner can view thier products    
class ViewProduct(View):
    
    def get(self, request, id, viewproductfitter = None):
        partner_id = Partner.objects.get(user__id = id)
        if viewproductfitter == None:
            all_product = Product.objects.filter(partner = partner_id)
        elif viewproductfitter == 'low':
            all_product = Product.objects.filter(partner = partner_id).order_by('price')
        elif viewproductfitter == 'high':
            all_product = Product.objects.filter(partner = partner_id).order_by('-price')
        elif viewproductfitter == 'mobile':
            all_product = Product.objects.filter(partner = partner_id).filter(category = 1)
        elif viewproductfitter == 'laptop':
            all_product = Product.objects.filter(partner = partner_id).filter(category = 2)
        elif viewproductfitter == 'topwear':
            all_product = Product.objects.filter(partner = partner_id).filter(category = 3)
        elif viewproductfitter == 'bottomwear':
            all_product = Product.objects.filter(partner = partner_id).filter(category = 4)
        return render(request, 'partners/viewproduct.html', {'allproduct': all_product, 'viewproductfitter': viewproductfitter})



def product_search(request):
    search_data = request.GET.get('productsearch')
    if search_data == '':
        return HttpResponse('Write something for search')
    id = request.user.id
    partner_id = Partner.objects.get(user__id = id)
    search_product = Product.objects.filter( Q(description__icontains = search_data) | Q(name__icontains = search_data), partner__exact = partner_id)
    return render(request, 'partners/searchproduct.html', {'search_product': search_product})

    
    
    
   
def delete_product(request, id):
    if request.method == 'POST':
        pi = Product.objects.get(pk = id)
        partner_id = pi.partner
        print(partner_id)
        pi.delete()
        #url = reverse('/partner/viewproduct/', kwargs = {'partner_id': partner_id})
        #return HttpResponseRedirect(url)
        return HttpResponseRedirect('/partner/viewproduct/<int:%s>/' % partner_id)

#function update and edit
def update_product(request, id):
    if request.method == 'POST':
        pi = Product.objects.get(pk = id)
        form = ProductAddForm(request.POST, instance = pi)
        if form.is_valid():
            form.save()
            messages.success(request, 'Congratulation!! updated Product Successfully')
           
    else:
        pi = Product.objects.get(pk = id)
        form = ProductAddForm(instance = pi)
    return render(request, 'partners/updateproduct.html', {'form': form})

#to show employee data
def employees_list(request, id):
    hired_by = Partner.objects.get(user__id = id)
    all_employees = Employee.objects.filter(hired_by = hired_by)
    return render(request, 'partners/viewemployees.html', {'allemployees': all_employees})

   
 
 #coupans
class AddCoupan(View):
  
    def get(self, request, id):
        if not request.user.user_type == 'Partner':
            return HttpResponseForbidden()
        form = AddCoupanForm()
        return render(request, 'partners/addcoupan.html' , {'form': form})
    
    def post(self, request, id):
        if not request.user.user_type == 'Partner':
                return HttpResponseForbidden()
        partner_id = Partner.objects.get(user__id = id)
        form = AddCoupanForm(request.POST)
        if form.is_valid():
            coupon_code = form.cleaned_data['coupon_code']
            coupon_percentage = form.cleaned_data['coupon_percentage']
            description = form.cleaned_data['description']
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            save_product = Coupon(partner = partner_id, coupon_code = coupon_code, coupon_percentage = coupon_percentage, description = description, start_date = start_date, end_date = end_date)
            save_product.save()
            messages.success(request, 'Congratulation!! Added Coupon Successfully')
            #form = AddCoupanForm()
        return render(request, 'partners/addcoupan.html', {'form':form})
    

# for view coupans
def view_coupan(request, id):
    partner_id = Partner.objects.get(user__id = id)
    if request.method == 'GET':
        all_coupan = Coupon.objects.filter(partner = partner_id)
        return render(request, 'partners/viewcoupan.html', {'allcoupan': all_coupan})
    
#delete coupans
def delete_coupan(request, id):
    if request.method == 'POST':
        ci = Coupon.objects.get(pk = id)
        partner_id = ci.partner
        print(partner_id)
        ci.delete()
        #url = reverse('/partner/viewcoupan/', kwargs = {'partner_id': partner_id})
        #return HttpResponseRedirect(url)
        return HttpResponseRedirect('/partner/viewcoupan/<int:%s>/' % partner_id)
    
    

  
    
def update_employee(request, id):
    pass

def delete_employee(request, id):
    pass


def partner_profile(request):
    return render(request, 'partners/profile.html')


