import employees
from django.urls import path
from partners import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.partner_dashboard, name='partnerdashboard'),
    path('logout/', auth_views.LogoutView.as_view(next_page = 'home'), name = 'logout'),
    path('addproduct/<int:id>/', views.AddProduct.as_view(), name='addproduct'),
    path('profile/<int:id>/', views.partner_profile, name = 'partnerprofile'),
    path('viewproduct/<int:id>/', views.ViewProduct.as_view(), name = 'viewproduct'),
    path('viewproduct/<int:id>/<slug:viewproductfitter>/', views.ViewProduct.as_view(), name = 'viewproductfitter'),
    path('productsearch/', views.product_search, name = 'productsearch'),
    
    path('deleteproduct/<int:id>/', views.delete_product, name = 'deleteproduct'),
    path('updateproduct/<int:id>/', views.update_product, name = 'updateproduct'),
    path('employeelist/<int:id>/' , views.employees_list, name = 'employeelist'),
    path('updateemployee/<int:id>', views.update_employee, name = 'updateemployee'),
    path('deleteemployee/<int:id>', views.delete_employee, name = 'deleteemployee'),
    path('addcoupan/<int:id>', views.AddCoupan.as_view(), name ='addcoupan'),
    path('viewcoupan/<int:id>',views.view_coupan, name = 'viewcoupan'),
    path('deletecoupan/<int:id>', views.delete_coupan, name = 'deletecoupan'),
    
   ] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)

