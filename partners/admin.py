from django.contrib.admin.decorators import register
from partners.models import Coupon, Category, Partner, Product
from django.contrib import admin

# Register your models here.


@admin.register(Partner)
class PartnerAdmin(admin.ModelAdmin):
    list_display = ('user', 'partner_rating')


@admin.register(Coupon)
class CouponAdmin(admin.ModelAdmin):
    list_display = ('partner', 'coupon_code', 'coupon_percentage', 'description', 'start_date', 'end_date')

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('category_name', 'description')
    
    
@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('partner','coupon', 'category', 'name', 'picture', 'rating', 'description', 'MFD', 'price')
     #list_display = ('partner', 'coupon', 'category', 'name', 'picture', 'rating', 'description', 'MFD', 'price')
    