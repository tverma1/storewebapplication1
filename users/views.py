from partners import forms
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.views import View
from .forms import RegistrationForm, UserProfileForm
from django.contrib import messages
from partners.models import Category, Product
from users.models import User



# home for all users
class HomeView(View):
    def get(self, request):
        top_wears = Category.objects.filter(category_name = 'Top Wear')
        bottom_wears = Category.objects.filter(category_name = 'Bottom Wear')
        mobiles = Product.objects.filter(category_id = 1)
        laptops = Category.objects.filter(category_name = 'Laptop')
        
        return render(request, 'home.html', {'topwears': top_wears, 'bottomwears': bottom_wears, 'mobiles': mobiles, 'laptops': laptops})
 
class RegistrationView(View):
    
    def get(self, request):
        form = RegistrationForm()
        return render(request, 'users/registration.html', {'form':form})
    
    def post(self, request):
        form = RegistrationForm(request.POST)
        if form.is_valid():
            messages.success(request, 'Congratulations !! Registered Successfully')
            form.save()
            form = RegistrationForm()
        return render(request, 'users/registration.html', {'form':form})
 
#check user type           
def user_check(request):
    if request.user.user_type == 'Customer':
        return HttpResponseRedirect('/')
    if request.user.user_type == 'Partner':
        return HttpResponseRedirect('/partner/')
    if request.user.user_type == 'Employee':
        return HttpResponseRedirect('/employee/')
    
        
# profile update    
class ProfileView(View):
    def get(self, request, id):
        pi = User.objects.get(pk = id)
        form = UserProfileForm(instance = pi)
        return render(request, 'users/profile.html' , {'form': form, 'active':'btn-primary'})
    
    def post(self, request, id):
        pi = User.objects.get(pk = id)
        form = UserProfileForm(request.POST, instance = pi)
        if form.is_valid():
            form.save()
            messages.success(request, 'Congratulation!! Profile Updated Successfully')
            #form = UserProfileForm()
        return render(request, 'users/profile.html', {'form':form, 'active':'btn-primary'})
    
    
def address(request):
 return render(request, 'users/address.html')

def change_password(request):
 return render(request, 'users/changepassword.html')

