from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from users.models import Address, User
# Register your models here.

admin.site.register(User, UserAdmin)
UserAdmin.fieldsets += ("Custom fields set", {'fields': ('profile_pic','mobile_number','user_type', 'gender')}),


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = ('user', 'home_street_no', 'city', 'state', 'country', 'zip_code')
   

