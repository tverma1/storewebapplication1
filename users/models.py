from storewebapplication1.settings import DEBUG
from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.

USER_CHOICES = (
    ('Partner', 'Partner'),
    ('Customer', 'Customer'),
    ('Employee', 'Employee'),
)

GENDER_CHOICES = (
    ('Female', 'Female'),
    ('Male', 'Male'),
)

class User(AbstractUser):
    profile_pic = models.ImageField(blank = True, null = True, upload_to = 'userimg')
    mobile_number = models.CharField(max_length = 50)
    user_type = models.CharField(max_length = 100, choices = USER_CHOICES, default = 'Customer')
    gender = models.CharField(max_length = 20, choices= GENDER_CHOICES)
    
    def __str__(self):
        return str(self.id)
    
    
class Address(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    home_street_no = models.IntegerField()
    city = models.CharField(max_length = 100)
    state = models.CharField(max_length = 100)
    country = models.CharField(max_length = 100)
    zip_code = models.IntegerField()
    
    def __str__(self):
        return str(self.id)
    
