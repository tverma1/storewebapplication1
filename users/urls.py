from django.urls import path, reverse_lazy
from users import views
from django.contrib.auth import views as auth_views
from .forms import LoginForm
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.HomeView.as_view(), name = 'home'),
    path('registration/', views.RegistrationView.as_view(), name='customerregistration'),
    path('accounts/login/', auth_views.LoginView.as_view(template_name = 'users/login.html', authentication_form = LoginForm), name = 'login'),
    path('usercheck/', views.user_check, name = 'usercheck'),
    path('logout/', auth_views.LogoutView.as_view(next_page = 'home'), name = 'logout'),
    path('profile/<int:id>/', views.ProfileView.as_view(), name = 'profile'),
    path('address/', views.address, name='address'),
    path('changepassword/', views.change_password, name='changepassword'),
   
    ] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)

