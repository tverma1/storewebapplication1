from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, UsernameField
from django.utils.translation import gettext, gettext_lazy as _
from users.models import User
from django.core import validators

class RegistrationForm(UserCreationForm):
    email = forms.CharField(label = 'Email', required = True, widget = forms.EmailInput(attrs={'class': 'form-control'}))
    password1 = forms.CharField(label= 'Password', widget = forms.PasswordInput(attrs = {'class': 'form-control'}))
    password2 = forms.CharField(label= 'Confirm Password', widget = forms.PasswordInput(attrs = {'class': 'form-control'}))
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
        widgets = {'username': forms.TextInput(attrs={'class':'form-control'})}


class LoginForm(AuthenticationForm):
    username = UsernameField(widget= forms.TextInput(attrs= {'autofocus': True, 'class':'form-control'}))
    password = forms.CharField(label=_('Password'), strip = False, widget = forms.PasswordInput(attrs= {'autocomplete': 'current-password', 'class':'form-control'}))
 

class UserProfileForm(forms.ModelForm):
    username = forms.CharField(label = 'User Name', widget = forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.CharField(label = 'Email', required = True, widget = forms.EmailInput(attrs={'class': 'form-control'}))
    first_name = forms.CharField(label = 'First Name', widget = forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(label = 'Last Name', widget = forms.TextInput(attrs={'class': 'form-control'}))
    #profile_pic = forms.ImageField(label = 'Profile Picture', widget = forms.ClearableFileInput(attrs={'class': 'form-control'}))
    mobile_number = forms.CharField(label = 'Moblie Number', validators = [validators.MaxLengthValidator(10),validators.MinLengthValidator(10)], widget = forms.TextInput(attrs={'class': 'form-control'}))
    #home_street_no = forms.IntegerField(label = 'h_noc', widget = forms.NumberInput(attrs={'class': 'form-control'}))
    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name', 'mobile_number', 'gender']
        
        widgets = {
            'gender': forms.Select(attrs={'class':'form-control'})
        }
        
