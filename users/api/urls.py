from django.urls import path, include

from users.api import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('api', views.UserViewSet, basename='userapi')
#router.register('api/<int:pk>/', views.CartViewSet, basename='cartapi')


urlpatterns = [
    path('', include(router.urls))
]