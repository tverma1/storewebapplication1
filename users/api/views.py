from rest_framework.response import Response
from  users.models import User
from users.api.serializers import UserSerializer
from rest_framework import viewsets
from rest_framework import status


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer 
 
 
'''   
class UserViewSet(viewsets.ViewSet):
    
    def list(self, request):
        user = User.objects.all()
        serializer = UserSerializer(user, many=True)
        return Response(serializer.data)
    
    def retrieve(self, request, pk=None):
        id = pk
        if id is not None:
            user = User.objects.get(id = id)
            serializer = UserSerializer(user)
            return Response(serializer.data)
        
    def create(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': ' user '}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)             
    
    def update(self, request, pk):
        user = pk
        user = User.objects.get(pk=user)
        serializer = UserSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'user completed updated in user'})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)             
    
    def partial_update(self, request, pk):
        user = pk
        user = User.objects.get(pk=user)
        serializer = UserSerializer(user, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'user updated partially '})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)    
    
    def destroy(self, request, pk):
        user = pk
        user = User.objects.get(pk=user)
        user.delete()
        return Response({'msg': 'user deleted'})
    '''    