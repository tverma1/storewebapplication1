from users.models import User
from rest_framework import serializers
from django.utils.timezone import now


class UserSerializer(serializers.ModelSerializer):
#class UserSerializer(serializers.HyperlinkedModelSerializer):
 
    day_since_joined = serializers.SerializerMethodField()
    
    def get_day_since_joined(self, obj):
        return (now() - obj.date_joined)
    class Meta:
        model = User
        fields = ['id', 'day_since_joined', 'username', 'email', 'first_name', 'last_name', 'mobile_number', 'gender']
        #fields = '__all__'