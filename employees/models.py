from partners.models import Partner
from django.db import models
from users.models import User
from partners.models import Partner

# Create your models here.


class Employee(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    hired_by = models.ForeignKey(Partner, on_delete = models.CASCADE, default = 1)
    salary = models.IntegerField()
    job_title = models.CharField(max_length= 50)
    DOJ = models.DateField()
    DOB = models.DateField()
    
    
class Attendance(models.Model):
    emp = models.ForeignKey(Employee, on_delete = models.CASCADE)
    work_day = models.IntegerField()
    check_in = models.TimeField()
    check_out = models.TimeField()
       

