from django.contrib import admin
from employees.models import Employee, Attendance

# Register your models here.
@admin.register(Employee)
class EmployeesAdmin(admin.ModelAdmin):
    list_display = ('user', 'salary','job_title', 'DOJ', 'DOB')

@admin.register(Attendance)
class AttendancesAdmin(admin.ModelAdmin):
    list_display = ('emp', 'work_day', 'check_in', 'check_out')
