from django.urls import path
from employees import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.employee_dashboard, name='employeedashboard'),
    path('logout/', auth_views.LogoutView.as_view(next_page = 'home'), name = 'logout'),
    
    
   ] 

