from django.http.response import  HttpResponseForbidden
from django.shortcuts import render
from partners.models import Partner, Product
from .forms import ProductAddForm
from django.contrib import messages
from django.views import View
from django.db.models import Q
#from django.core.urlresolvers import reverse

# Create your views here.

def partner_dashboard(request):
    return render(request, 'partners/partnerdashboard.html')

# adding product
class AddProduct(View):
  
    def get(self, request, id):
        if not request.user.user_type == 'Partner':
            return HttpResponseForbidden()
        form = ProductAddForm()
        return render(request, 'partners/addproduct.html' , {'form': form})
    
    def post(self, request, id):
        if not request.user.user_type == 'Partner':
                return HttpResponseForbidden()
        partner_id = Partner.objects.get(user__id = id)
        form = ProductAddForm(request.POST, request.FILES)
        if form.is_valid():
            coupon = form.cleaned_data['coupon']
            category = form.cleaned_data['category']
            name = form.cleaned_data['name']
            picture = form.cleaned_data['picture']
            description = form.cleaned_data['description']
            MFD = form.cleaned_data['MFD']
            price = form.cleaned_data['price']
            save_product = Product(partner = partner_id, coupon = coupon, category = category, name = name, picture = picture, description = description, MFD = MFD, price = price)
            save_product.save()
            messages.success(request, 'Congratulation!! Added Product Successfully')
            form = ProductAddForm()
        return render(request, 'partners/addproduct.html', {'form':form})
    
# partner can view thier products    
class ViewProduct(View):
    
    def get(self, request, id, viewproductfitter = None):
        partner_id = Partner.objects.get(user__id = id)
        if viewproductfitter == None:
            all_product = Product.objects.filter(partner = partner_id)
        elif viewproductfitter == 'low':
            all_product = Product.objects.filter(partner = partner_id).order_by('price')
        elif viewproductfitter == 'high':
            all_product = Product.objects.filter(partner = partner_id).order_by('-price')
        elif viewproductfitter == 'mobile':
            all_product = Product.objects.filter(partner = partner_id).filter(category = 1)
        elif viewproductfitter == 'laptop':
            all_product = Product.objects.filter(partner = partner_id).filter(category = 2)
        elif viewproductfitter == 'topwear':
            all_product = Product.objects.filter(partner = partner_id).filter(category = 3)
        elif viewproductfitter == 'bottomwear':
            all_product = Product.objects.filter(partner = partner_id).filter(category = 4)
        return render(request, 'partners/viewproduct.html', {'allproduct': all_product, 'viewproductfitter': viewproductfitter})




    
   
