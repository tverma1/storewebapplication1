from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import redirect, render
from django.views import View
from partners.models import Product
from .models import Cart, Order
from users.models import Address
from decimal import Decimal
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
#from .tasks import test_func


from celery.schedules import crontab
from customers.tasks import send_mail_func
#from django_celery_beat.models import PeriodicTask, CrontabSchedule
import json

# Create your views here.

# show product detail
class ProductDetail(View):
    def get(self, request, pk):
        product = Product.objects.get(pk = pk)
        if product.coupon:
            coupan_id = product.coupon
            coupan_per = Decimal(coupan_id.coupon_percentage)/100
            discounted_amount = product.price * coupan_per
            discounted_price = product.price - discounted_amount
            print(coupan_per)
        else:
            discounted_price = product.price
        item_already_in_cart = False
        if request.user.is_authenticated:
            item_already_in_cart = Cart.objects.filter(Q(product = product.id) & Q(user = request.user)).exists()
        return render(request, 'customers/productdetail.html', {'product' : product, 'item_already_in_cart': item_already_in_cart, 'discounted_price':discounted_price})

#show mobiles
def mobile(request, mobiledatafilter = None):
    if mobiledatafilter == None:
        mobiles = Product.objects.filter(category = 1)
    elif mobiledatafilter == 'below':
        mobiles = Product.objects.filter(category = 1).filter(price__lt = 20000)
    elif mobiledatafilter == 'above':
        mobiles = Product.objects.filter(category = 1).filter(price__gt = 20000)
    elif mobiledatafilter == 'low':
        mobiles = Product.objects.filter(category = 1).order_by('price')
    elif mobiledatafilter == 'high':
        mobiles = Product.objects.filter(category = 1).order_by('-price')
    return render(request, 'customers/mobile.html', {'mobiles': mobiles})

@login_required
def add_to_cart(request):
    user = request.user
    product_id = request.GET.get('product_id')
    discounted_price = request.GET.get('discounted_price')
    product = Product.objects.get(id = product_id)
    #coupon = Product.objects.get(coupon = product_id)
    coupon = product.coupon
    Cart(user = user, product = product, coupan = coupon, discounted_price = discounted_price).save()
    return redirect('/customer/showcart/')


@login_required
def show_cart(request):
    if request.user.is_authenticated:
        user = request.user
        cart = Cart.objects.filter(user = user)
        amount = Decimal(0.00)
        shipping_amount = Decimal(50.00)
        total_amount = Decimal(0.00)
        cart_product = [p for p in Cart.objects.all() if p.user == user]
        if cart_product:
            for p in cart_product:
                temp_amount = (p.quantity * p.discounted_price)
                amount += temp_amount
            total_amount = amount + shipping_amount
            return render(request, 'customers/addtocart.html', {'carts': cart, 'totalamount': total_amount, 'amount':amount})
        else:
            return render(request, 'customers/emptycart.html')
       
#add quantity
def plus_cart(request):
    if request.method == 'GET':
        prod_id = request.GET['prod_id']
        print(prod_id)
        c = Cart.objects.get(Q(product = prod_id) & Q(user = request.user))
        c.quantity += 1
        c.save()
        amount = Decimal(0.00)
        shipping_amount = Decimal(50.00)
        cart_product = [p for p in Cart.objects.all() if p.user == request.user]
        for p in cart_product:
            temp_amount = (p.quantity * p.discounted_price)
            amount += temp_amount
        data = {
            'quantity': c.quantity,
            'amount': amount,
            'totalamount': amount + shipping_amount,
            }
        return JsonResponse(data)
        
        
# minus quantity
def minus_cart(request):
    if request.method == 'GET':
        prod_id = request.GET['prod_id']
        print(prod_id)
        c = Cart.objects.get(Q(product = prod_id) & Q(user = request.user))
        c.quantity -= 1
        c.save()
        amount = Decimal(0.00)
        shipping_amount = Decimal(50.00)
        cart_product = [p for p in Cart.objects.all() if p.user == request.user]
        for p in cart_product:
            temp_amount = (p.quantity * p.discounted_price)
            amount += temp_amount
        data = {
            'quantity': c.quantity,
            'amount': amount,
            'totalamount': amount + shipping_amount,
            }
        return JsonResponse(data)

#remove cart 
def remove_cart(request):
    if request.method == 'GET':
        prod_id = request.GET['prod_id']
        c = Cart.objects.get(Q(product = prod_id) & Q(user = request.user))
        c.delete()
        amount = Decimal(0.00)
        shipping_amount = Decimal(50.00)
        cart_product = [p for p in Cart.objects.all() if p.user == request.user]
        for p in cart_product:
            temp_amount = (p.quantity * p.discounted_price)
            amount += temp_amount
        data = {
            'amount': amount,
            'totalamount': amount + shipping_amount,
            }
        return JsonResponse(data)
 
 
def buy_now(request):
 return render(request, 'customers/buynow.html')

@login_required
def orders(request):
    order = Order.objects.filter(user = request.user)
    return render(request, 'customers/orders.html', {'order': order})


@login_required
def checkout(request):
    user = request.user
    address = Address.objects.filter(user = user)
    cart_item = Cart.objects.filter(user = user)
    amount = amount = Decimal(0.00)
    shipping_amount = Decimal(50.00)
    totalamount = Decimal(0.0)
    cart_product = [p for p in Cart.objects.all() if p.user == request.user]
    if cart_product:
        for p in cart_product:
            temp_amount = (p.quantity * p.discounted_price)
            amount += temp_amount
        totalamount = amount + shipping_amount
    return render(request, 'customers/checkout.html', {'address': address, 'totalamount': totalamount, 'cartitem': cart_item})

#payment done
@login_required
def payment_done(request):
    user = request.user
    cart = Cart.objects.filter(user = user)
    for c in cart:
        Order(user = user, product = c.product, quantity = c.quantity, discounted_price = c.discounted_price).save()
        c.delete()
    return redirect('/customer/sendmail')


'''
#celery
def test(request):
    test_func.delay()
    return HttpResponse("Done")
    '''

#celery send mail
def send_mail(request):
    user = request.user
    send_mail_func.delay(user.id)
    return HttpResponse("Sent")
