from django.urls import path, reverse_lazy
from django.urls.conf import include
from customers import views
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
path('mobile/', views.mobile, name='mobile'),
path('mobile/<slug:mobiledatafilter>/', views.mobile, name='mobiledatafilter'),
path('productdetail/<int:pk>/', views.ProductDetail.as_view(), name = 'productdetail'),
path('add-to-cart/', views.add_to_cart, name='add-to-cart'),
path('showcart/', views.show_cart, name = 'showcart'),
path('pluscart/', views.plus_cart, name = 'pluscart'),
path('minuscart/', views.minus_cart, name = 'minuscart'),
path('removecart/', views.remove_cart, name = 'removecart'),
path('checkout/', views.checkout, name='checkout'),
path('paymentdone/', views.payment_done, name = "paymentdone"),
path('buy/', views.buy_now, name='buy-now'),
path('orders/', views.orders, name='orders'),
path('sendmail/', views.send_mail, name = 'sendmail'),


]

#path('test/', views.test, name = 'tset')
