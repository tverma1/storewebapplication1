from django.contrib import admin
from  customers.models import Order, Cart

# Register your models here.


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('product', 'user', 'quantity', 'order_date', 'status')
    
@admin.register(Cart)
class CartAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'product','coupan', 'quantity', 'discounted_price')