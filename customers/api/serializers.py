from customers.models import Cart, Order
from rest_framework import serializers


class CartSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Cart
        fields = ['id', 'user', 'product', 'coupan', 'quantity', 'discounted_price']