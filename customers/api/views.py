from rest_framework.response import Response
from customers.models import Cart, Order
from customers.api.serializers import CartSerializer
from rest_framework import serializers, viewsets
from decimal import Decimal
from rest_framework import status
from django.shortcuts import redirect, render
from django.db.models import Q

'''
class CartViewSet(viewsets.ModelViewSet):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer 
    '''
    
class CartViewSet(viewsets.ViewSet):
  
    def list(self, request):
        cart = Cart.objects.all()
        serializer = CartSerializer(cart, many=True)
        return Response(serializer.data)
    
    '''def retrieve(self, request, pk=None):
        user = pk
        if user is not None:
            cart = Cart.objects.filter(user=user)
            serializer = CartSerializer(cart, many=True)
            return Response(serializer.data)'''
            
    def retrieve(self, request, pk=None):
        prod_id = pk
        if prod_id is not None:
            cart = Cart.objects.get(pk=prod_id)
            serializer = CartSerializer(cart, many=True)
            return Response(serializer.data)
        
    def create(self, request):
        serializer = CartSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'product added in cart'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)             
    
    def update(self, request, pk):
        prod_id = pk
        cart = Cart.objects.get(user=prod_id)
        serializer = CartSerializer(cart, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'product updated in cart'})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)             
    
    def partial_update(self, request, pk):
        prod_id = pk
        cart = Cart.objects.get(pk=prod_id)
        serializer = CartSerializer(cart, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'product updated partially in cart'})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)    
    
    def destroy(self, request, pk):
        prod_id = pk
        cart = Cart.objects.get(product=prod_id)
        cart.delete()
        return Response({'msg': 'cart deleted'})
        