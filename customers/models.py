from django.db import models
from partners.models import Coupon, Product
from users.models import User

# Create your models here.


STATUS_CHOICES = (
    ('Accepted', 'Accepted'),
    ('Packed', 'Packed'),
    ('On The Way', 'On The Way'),
    ('Delivered', 'Delivered'),
    ('Cancel', 'Cancel'),
)

class Order(models.Model):
    product = models.ForeignKey(Product, on_delete = models.CASCADE)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    quantity = models.PositiveIntegerField(default = 1)
    order_date = models.DateTimeField(auto_now_add = True)
    status = models.CharField(max_length = 20, choices = STATUS_CHOICES, default = 'Pending')
    discounted_price = models.DecimalField(max_digits = 10, decimal_places = 2, default=1)
    
    def __str__(self):
        return str(self.id)
    
    @property
    def order_total_cost(self):
        return self.quantity * self.discounted_price
    

class Cart(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    product = models.ForeignKey(Product, on_delete = models.CASCADE)
    coupan = models.ForeignKey(Coupon, on_delete = models.CASCADE, null = True)
    quantity = models.PositiveIntegerField(default = 1)
    discounted_price = models.DecimalField(max_digits = 10, decimal_places = 2)
    
    def __str__(self):
        return str(self.id)
    
    @property
    def total_cost(self):
        return self.quantity * self.discounted_price
    
    @property
    def original_cost(self):
        return self.quantity * self.product.price    