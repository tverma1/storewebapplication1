from users.models import User
from django.contrib.auth import get_user_model
from celery import shared_task
from django.core.mail import send_mail
from storewebapplication1 import settings
from django.utils import timezone
from datetime import timedelta
'''
@shared_task(bind = True)
def test_func(self):
    for i in  range(10):
        print(i)
    return "Done"
    '''
    
@shared_task(bind=True)
def send_mail_func(self, id):
    users = get_user_model().objects.get(user = id)
    #timezone.localtime(users.date_time) + timedelta(days=2)
    for user in users: 
        mail_subject = "Hi! Taruna"
        message = "your order placed"
        to_email = user.email
        send_mail(
            subject = mail_subject,
            message = message,
            from_email = settings.EMAIL_HOST_USER,
            recipient_list = [to_email],
            fail_silently = True,
        )
    return "Done"